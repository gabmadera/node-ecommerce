module.exports = {
  env: {
    commonjs: true,
    es2020: true,
    node: true,
  },
  extends: ['prettier', 'airbnb-base'],
  plugins: ['prettier'],
  parserOptions: {
    ecmaVersion: 11,
  },
  rules: {
    semi: 0,
    'no-console': 0,
    'comma-dangle': 0,
    'consistent-return': 0,
    'no-underscore-dangle': 0,
    'function-paren-newline': 0,
  },
}
