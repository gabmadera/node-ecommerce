const express = require('express')
const passport = require('passport')

const router = express.Router()

router.get('/check-session', (req, res, next) => {
  if (req.user) {
    return res.status(200).json({ data: req.user })
  }
  const err = new Error('No user session found!')
  err.code = 401
  return next(err)
})

router.post('/login', (req, res, next) => {
  passport.authenticate('login', (error, user) => {
    if (error) {
      return res.status(500).json({ message: error.message })
    }
    req.logIn(user, (err) => {
      if (err) {
        return res.status(500).json({ message: error.message })
      }

      return res.status(200).json({ data: 'Glad to have you back' })
    })
  })(req, res, next)
})

router.post('/signup', (req, res, next) => {
  passport.authenticate('signup', (error, user) => {
    if (error) {
      return res.status(500).json({ message: error.message })
    }

    req.logIn(user, (err) => {
      if (err) {
        return res.status(500).json({ message: error.message })
      }

      return res.status(200).json({ data: 'Welcome aboard' })
    })
  })(req, res, next)
})

// eslint-disable-next-line no-unused-vars
router.post('/logout', (req, res, _next) => {
  if (req.user) {
    req.logout()

    req.session.destroy((err) => {
      if (err) {
        return res.status(500).json({ message: err.message })
      }
      res.clearCookie('connect.sid')

      return res.status(200).json({ data: 'Bye bye' })
    })
  } else {
    return res.status(200).json({ data: 'No session!' })
  }
})

module.exports = router
