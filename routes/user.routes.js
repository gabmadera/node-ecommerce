const router = require('express').Router()

const User = require('../models/User')

const { isCurrentUser } = require('../middlewares/auth.middleware')

// View profile
router.get('/:userId', [isCurrentUser], async (req, res, next) => {
  try {
    const { userId } = req.params

    const user = await User.find({ _id: userId }, { username: 1, email: 1, _id: 0 })
    return res.status(200).json({ data: user })
  } catch (err) {
    return next(err)
  }
})

// Check-Cart
router.get('/check-cart/:userId', [isCurrentUser], async (req, res, next) => {
  try {
    const { userId } = req.params

    const cart = await User.findById(userId).populate('cart')
    return res
      .status(200)
      .json({ message: 'Here are the items you want to purchase', data: cart.cart })
  } catch (err) {
    return next(err)
  }
})

module.exports = router
