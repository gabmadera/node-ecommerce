const router = require('express').Router()

const Product = require('../models/Product')
const User = require('../models/User')

const { isCurrentUser } = require('../middlewares/auth.middleware')

// TODO requerir los middlewares de auth y usuarios

// RUTAS

// ruta sin auth (cualquiera podra ver los productos)
router.get('/all', async (req, res, next) => {
  const { page = 1, limit = 10 } = req.query

  try {
    const products = await Product.find()
      .limit(limit * 1)
      .skip((page - 1) * limit)
      .exec()

    const count = await Product.countDocuments()

    return res
      .status(200)
      .json({ totalPages: Math.ceil(count / limit), currentPage: page, data: products })
  } catch (err) {
    return next(err)
  }
})

// ruta por nombre
// ! YEAAHHH Funciona, devuelve los productos que contengan el parametro de busqueda
router.get('/search/:name', async (req, res, next) => {
  try {
    const productName = req.params.name

    const product = await Product.find(
      { name: { $regex: productName, $options: 'i' } },
      // eslint-disable-next-line object-curly-newline
      { name: 1, price: 1, description: 1, image: 1, _id: 0 }
    )

    return res.status(200).json({ data: product })
  } catch (error) {
    return next(error)
  }
})

// ruta por precio
// ! YEAAHHH Funciona el sorting por precio
// REGRESA TODOS LOS PRODUCTOS DE VALOR MENOR AL PRECIO BUSCADO
router.get('/sort/:price', async (req, res, next) => {
  try {
    const productPrice = req.params.price
    const products = await Product.find({
      price: {
        $lte: productPrice,
      },
    })
    console.log(products)

    // const maxPrice = Object.keys(products).filter((price) => price <= productPrice)
    // console.log(maxPrice)

    return res
      .status(200)
      .json({ message: 'HERE ARE THE PRODUCTS UNDER THE DESIRED PRICE', data: products })
  } catch (error) {
    return next(error)
  }
})

// !AGREGAR AL CARRITO
router.put('/add-to-cart', [isCurrentUser], async (req, res, next) => {
  try {
    const { userId } = req.body
    const { productId } = req.body

    const updatedCart = await User.findByIdAndUpdate(
      userId,
      { $push: { cart: productId } },
      { new: true }
    )

    return res.status(200).json({ message: 'Item added to your cart!', data: updatedCart })
  } catch (error) {
    return next(error)
  }
})

module.exports = router
