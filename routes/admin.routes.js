const router = require('express').Router()
const lodash = require('lodash')

const Product = require('../models/Product')
const User = require('../models/User')

const { isAdminRole } = require('../middlewares/auth.middleware')
const { imageUpload, uploadToCloudinary } = require('../middlewares/file.middleware')

// Endpoints de informacion para el admin
// TODO me dice que no soy el usuario actual
router.get('/', async (req, res, next) => {
  const { page = 1, limit = 10 } = req.query

  try {
    const products = await Product.find()
      .limit(limit * 1)
      .skip((page - 1) * limit)
      .exec()

    const count = await Product.countDocuments()

    return res
      .status(200)
      .json({ totalPages: Math.ceil(count / limit), currentPage: page, data: products })
  } catch (err) {
    return next(err)
  }
})

router.get('/users', [isAdminRole], async (req, res, next) => {
  try {
    const users = await User.find()
    return res.status(200).json({ data: users })
  } catch (err) {
    return next(err)
  }
})

// Endpoints de edicion y crwacion de productos...

// New product
router.post(
  '/new-item',
  [imageUpload.single('image'), uploadToCloudinary],
  async (req, res, next) => {
    try {
      const newItem = new Product({
        name: req.body.name,
        price: req.body.price,
        description: req.body.description,
        // image: req.body.image,
        image: req.file ? req.file.filePath : null,
      })

      await newItem.save().then(() =>
        // eslint-disable-next-line implicit-arrow-linebreak
        res.status(200).json({ message: 'New item correctly added to DB', data: newItem })
      )
    } catch (err) {
      return next(err)
    }
  }
)

// Edit product
router.put(
  '/edit/:productId',
  [imageUpload.single('image'), uploadToCloudinary],
  async (req, res, next) => {
    try {
      const product = req.params.productId

      const newFields = {
        name: req.body.name,
        price: req.body.price,
        description: req.body.description,
        image: req.file ? req.file.filePath : null,
      }

      // LONG LIVE LODASH
      const cleanProduct = lodash.pickBy(newFields, (value) => !!value)

      const editedProduct = await Product.findByIdAndUpdate(product, cleanProduct, { new: true })

      return res.status(200).json({ message: 'Product updated correctly', data: editedProduct })
    } catch (err) {
      return next(err)
    }
  }
)

// Delete product
router.delete('/:id', async (req, res, next) => {
  try {
    const { id } = req.params

    await Product.findByIdAndDelete(id)
    return res.status(200).json({ data: 'Product Sucessfuly Removed' })
  } catch (error) {
    return next(error)
  }
})

module.exports = router
