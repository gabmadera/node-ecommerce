const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const bcrypt = require('bcrypt')

const User = require('../models/User')

const SALT_ROUNDS = 12

// Metodo de registro
passport.use(
  'signup',
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true,
    },
    async (req, email, password, done) => {
      try {
        if (req.body.role && req.body.role !== 'basic') {
          const error = new Error('Invalid role')
          return done(error, null)
        }
        const prevUser = await User.findOne({ email: email.toLowerCase() })

        if (prevUser) {
          const error = new Error('This user is already registered.')
          return done(error, null)
        }

        const passValid = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(password)

        if (!passValid) {
          const error = new Error(
            'Invalid password, must be minimum 8 characters and use at least a letter and a number'
          )
          return done(error, null)
        }

        const hash = await bcrypt.hash(password, SALT_ROUNDS)

        const newUser = new User({
          username: req.body.username,
          // eslint-disable-next-line object-shorthand
          email: email,
          password: hash,
        })

        const savedUser = await newUser.save()

        done(null, savedUser)
      } catch (err) {
        return done(err, null)
      }
    }
  )
)

// Metodo de login
passport.use(
  'login',
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
    },
    async (email, password, done) => {
      try {
        const currentUser = await User.findOne({ email })

        if (!currentUser) {
          const error = new Error('Username or password incorrect')
          return done(error, null)
        }

        const passValidation = await bcrypt.compare(password, currentUser.password)

        if (!passValidation) {
          const error = new Error('Username or password incorrect')
          return done(error, null)
        }

        done(null, currentUser)
      } catch (err) {
        return done(err, null)
      }
    }
  )
)

passport.serializeUser((user, done) => done(null, user._id))

passport.deserializeUser(async (userId, done) => {
  try {
    const existingUser = await User.findById(userId)
    return done(null, existingUser)
  } catch (err) {
    return done(err)
  }
})

module.exports = passport
