require('dotenv').config()

const express = require('express')
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)
const mongoose = require('mongoose')

require('./config/db')
const passport = require('./config/passport')
const authRouter = require('./routes/auth.routes')
const productRoutes = require('./routes/product.routes')
const adminRoutes = require('./routes/admin.routes')
const userRoutes = require('./routes/user.routes')

const { isAuthenticated, isAdminRole } = require('./middlewares/auth.middleware')
const errorMiddleware = require('./middlewares/error.middleware')

const server = express()

server.use(express.json())
server.use(express.urlencoded({ extended: false }))

server.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 3600000,
    },
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
  })
)

server.use(passport.initialize())
server.use(passport.session())

server.use('/api/auth', authRouter)

server.use('/api/products', productRoutes)
server.use('/api/admin', [isAuthenticated, isAdminRole], adminRoutes)
server.use('/api/user', [isAuthenticated], userRoutes)

server.use(errorMiddleware)

const { PORT } = process.env
server.listen(PORT, () => {
  console.log(`Server running in http://localhost:${PORT}`)
})
