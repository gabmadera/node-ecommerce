const mongoose = require('mongoose')

const { Schema, model } = mongoose

const productSchema = new Schema({
  name: { type: String, required: true },
  price: { type: Number, required: true },
  description: { type: String, required: true },
  image: { type: String },
})

const Product = model('Product', productSchema)

module.exports = Product
