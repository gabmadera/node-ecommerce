const mongoose = require('mongoose')

const { Schema, model } = mongoose

const userSchema = new Schema(
  {
    username: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true, minlength: 8 },
    cart: [{ type: mongoose.Types.ObjectId, ref: 'Product' }],
    role: { type: String, default: 'basic' },
  },
  {
    timestamps: true,
    // eslint-disable-next-line comma-dangle
  }
)

const User = model('User', userSchema)

module.exports = User
