require('dotenv').config()

const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const User = require('../models/User')

const SALT_ROUNDS = 10
const passwordHash = bcrypt.hashSync('v21015440', SALT_ROUNDS)

const userSeed = [
  {
    username: 'ADMIN',
    email: 'admin@upgrade.com',
    password: passwordHash,
    role: 'admin',
  },
]

const userInstances = userSeed.map((user) => new User(user))

const { NODE_ENV, DB_URI_DEV, DB_URI_PROD } = process.env

const DB_URL = NODE_ENV === 'production' ? DB_URI_PROD : DB_URI_DEV
// const DB_URL = DB_URI_PROD
// const DB_URL = DB_URI_DEV

mongoose
  .connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .then(async () => {
    console.log('Connected to DB')

    const user = await User.find()
    console.log('Usuarios: ', user)

    if (user.length) {
      console.log('Dropping...')
      await User.collection.drop()
      console.log('Dropped!')
    }

    console.log('Saving users...')
    await User.insertMany(userInstances)
    console.log('Users saved!')
  })
  .catch((err) => {
    console.error(err.message)
    console.error('Conection error')
  })
  .finally(() => {
    mongoose.disconnect()
  })
