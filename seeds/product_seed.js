require('dotenv').config()

const mongoose = require('mongoose')

const Product = require('../models/Product')

const productSeed = [
  {
    name: 'Fancy Watch',
    price: 200,
    description: 'A cool watch, cannot show time but it looks cool',
    image: 'https://www.roudstudio.com/images/works/product-photo/img07.jpg',
  },
  {
    name: 'Cosmetics Kit',
    price: 30,
    description: 'throw it at yourself to smell nice',
    image: 'https://www.yourcamerastory.com/wp-content/uploads/2019/07/product-photo-styling.jpg',
  },
  {
    name: 'Handbag',
    price: 50,
    description: 'meh, not that pretty',
    image: 'https://1011707145.rsc.cdn77.org/data/_galerie_media/f_f-1-kabelky-26112013-1.jpg',
  },
  {
    name: 'Ninja Knife',
    price: 1500,
    description: 'Used by the legendary fruit ninja',
    image:
      'https://www.dusanholovej.com/assets/images/7/PRODUCT-PHOTOGRAPHY-SEKIMAGOROKU-JAPANESE-KNIFE-1d19ff84.jpg',
  },
  {
    name: 'Face Lotion',
    price: 10,
    description: 'Like a rock',
    image: 'https://i.pinimg.com/236x/1d/8f/d3/1d8fd3eb268bb62d0e8456a8db6f11d3.jpg',
  },
  {
    name: 'Infinite Ice',
    price: 4,
    description: 'Will last forever',
    image: 'https://images.lucepictor.com/wp-content/uploads/2016/09/ice-splash-product-photo.jpg',
  },
  {
    name: 'Fancy Watch 2',
    price: 200,
    description: 'Different, but the same',
    image: 'https://www.roudstudio.com/images/works/product-photo/img07.jpg',
  },
  {
    name: 'Fancy Watch 3',
    price: 200,
    description: 'The same, but different',
    image:
      'https://cococreativestudio.com/wp-content/uploads/2018/09/Daniel-wellington-DW-watch-watches-photography-products-singapore-montre-4-copy.jpg',
  },
  {
    name: 'Ugly Shoes',
    price: 20,
    description: 'So ugly you can take them for free',
    image:
      'https://artandcreativity.com/wp-content/uploads/2020/04/2019-04-20-product2014-scaled.jpg',
  },
  {
    name: 'Coca-Cola',
    price: 1.2,
    description: 'Because yes',
    image:
      'https://mir-s3-cdn-cf.behance.net/project_modules/disp/ba93de16379353.562aaddef374a.jpg',
  },
  {
    name: 'A Camera',
    price: 400,
    description: 'Exclusively for selfies',
    image:
      'https://s23527.pcdn.co/wp-content/uploads/2017/12/cblurMEDIAphotography.com_DIYP__SNY8695-745x497.jpg.optimal.jpg',
  },
  {
    name: 'WTF is this?',
    price: 0.5,
    description: 'Soooo useless',
    image: 'https://cdn2.f-cdn.com/contestentries/1260255/24259882/5a92cc12f090a_thumb900.jpg',
  },
  {
    name: 'Speakers',
    price: 50,
    description: 'They do NOT speak',
    image:
      'https://www.53ne.com/wp-content/uploads/2019/01/nomodo-speaker-warmer-gadget-product-photographer-michigan-detroit-advertising-lifestyle-ideas-unique-moody-cool01.jpg',
  },
  {
    name: 'Ugly Amazon Echo',
    price: 49.99,
    description: 'Something weird',
    image: 'https://www.roudstudio.com/images/works/product-photo/img07.jpg',
  },
  {
    name: 'LOLLLL',
    price: 25,
    description: 'Come on man...',
    image:
      'https://lh3.googleusercontent.com/proxy/XLHs4-DIranUvLulveJ-ifNYeyLFngc90uOclhStT1dFXlQuZB42Sy_3rG9v9RvSbSz-Cj_H2kHrn7SaTSOqyu56U9qzrpR4H15QyyXJR19oQqClZiKthDV5XpGXy7nJ2AHr38bDODiWQyJ-q7TOKd8',
  },
  {
    name: 'Explosive Shoes',
    price: 34,
    description: 'It will explode',
    image:
      'https://images.squarespace-cdn.com/content/v1/51ce36b1e4b05d425c820ea0/1562178059350-7BEW3E318LRGNIFRIGO0/ke17ZwdGBToddI8pDm48kGwqNa-TSATgABi909OK27Z7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UQSxQa_pE67Ig1CszvlZo11NCLvqIlshiNC_JCcjnOmqOV4zqrbdg_2AqIEjj1Z3Fg/South-Bend-Indiana-product-photography-for-advertising-brian-rodgers-jr-digital-art-that-rocks.jpg?format=2500w',
  },
  {
    name: 'Goggles',
    price: 213,
    description: 'Not Googles, Goggles',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQWupUeX_tWcWWgHPQDSZHNWjZuwg4u57n99WId-pxnj8RfT1lq&usqp=CAU',
  },
  {
    name: 'Running Out of Ideas',
    price: 200,
    description: 'Make it staaahp',
    image:
      'https://images.squarespace-cdn.com/content/v1/542e8709e4b0f766b2e91e0b/1558599357456-BCCHZIIHLZFK7X0D9HS4/ke17ZwdGBToddI8pDm48kDNFk7ryRAtlK1uUMMbjvWZ7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QHyNOqBUUEtDDsRWrJLTmmt9rVnoh5SsI9ugICaYdmQeOQ88ovd8295MlQVJbZOvk6CUk2vdwtvbR5MgylyMZ/Shoes_Product+_Photography_6.jpg',
  },
  {
    name: 'Gummy Bears',
    price: 200,
    description: 'Lorem freaking Ipsum',
    image: 'https://learn.zoner.com/wp-content/uploads/2015/02/header2.jpg',
  },
  {
    name: 'Blue shit',
    price: 200,
    description: 'Blue shit',
    image: 'https://www.theartisan.es/wp-content/uploads/2015/06/MG_3160-705x529.jpg',
  },
]

const productInstances = productSeed.map((product) => new Product(product))

const { NODE_ENV, DB_URI_DEV, DB_URI_PROD } = process.env

const DB_URL = NODE_ENV === 'production' ? DB_URI_PROD : DB_URI_DEV

// const DB_URL = DB_URI_PROD
// const DB_URL = DB_URI_DEV

mongoose
  .connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(async () => {
    console.log('Connected to DB')

    const products = await Product.find()
    console.log('Some products: ', products)

    if (products.length) {
      console.log('Drop to empty just in case')
      await Product.collection.drop()
      console.log('Products emptied')
    }

    console.log('Saved')
    await Product.insertMany(productInstances)
    console.log('Done')
  })
  .catch((err) => {
    console.error(err.message)

    console.error('Could not connect to dB')
  })
  .finally(() => mongoose.disconnect())
