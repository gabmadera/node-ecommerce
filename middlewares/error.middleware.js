// eslint-disable-next-line no-unused-vars
function errorMiddleware(err, _req, res, _next) {
  return res.status(err.code || 500).json({
    message: err.message || 'Unexpected error',
  })
}

module.exports = errorMiddleware
