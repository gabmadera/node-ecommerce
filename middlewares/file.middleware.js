const multer = require('multer')
const path = require('path')

const fs = require('fs')
const cloudinary = require('cloudinary').v2

const diskStorage = multer.diskStorage({
  filename: (req, file, done) => {
    done(null, Date.now() + path.extname(file.originalname))
  },
  destination: (req, file, done) => {
    done(null, path.join(__dirname, '../public/uploads'))
  },
})

const VALID_FILE_TYPES = ['image/png', 'image/jpg']

const imageFileFilter = (req, file, done) => {
  if (!VALID_FILE_TYPES.includes(file.mimetype)) {
    done(new Error('Invalid file type'))
  } else {
    done(null, true)
  }
}

const uploadToCloudinary = async (req, res, next) => {
  try {
    if (req.file) {
      const filePath = req.file.path
      const image = await cloudinary.uploader.upload(filePath)

      req.file.filePath = image.secure_url

      await fs.unlinkSync(filePath)
    }
    return next()
  } catch (err) {
    req.file.filePath = `http://localhost:${process.env.PORT}/uploads/${req.file.filename}`
    return next()
  }
}

const imageUpload = multer({
  storage: diskStorage,
  fileFilter: imageFileFilter,
})

module.exports = { imageUpload, uploadToCloudinary }
