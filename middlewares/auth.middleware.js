function isAuthenticated(req, res, next) {
  if (req.user) {
    return next()
  }
  const error = new Error('Forbidden route! You need to be logged in!')
  error.code = 401
  return next(error)
}

function isBasicRole(req, res, next) {
  if (req.user.role === 'basic') {
    return next()
  }
  const error = new Error('Forbidden')
  error.code = 401
  return next(error)
}

function isAdminRole(req, res, next) {
  if (req.user.role === 'admin') {
    return next()
  }
  const error = new Error('YOU SHALL NOT PASS')
  error.code = 401
  return next(error)
}

function isCurrentUser(req, res, next) {
  if (String(req.user._id) === req.body.userId || req.params.userId) {
    return next()
  }
  const error = new Error('You are not who you say you are')
  error.code = 403
  return next(error)
}

module.exports = {
  isAuthenticated,
  isBasicRole,
  isAdminRole,
  isCurrentUser,
}
